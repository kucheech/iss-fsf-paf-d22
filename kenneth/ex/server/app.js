const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");
const Sequelize = require('sequelize');

const DB_NAME = "employees";
const DB_USER = "root";
const DB_PASSWORD = "password";
const DB_HOST = "localhost"

const conn = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD,
    {
        host: DB_HOST,
        dialect: 'mysql',
        logging: console.log,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        define: {
            timestamps: false
        }
    }
);

// const Departments = sequelize.define('departments', {
//     dept_no: {
//         type: Sequelize.STRING,
//         primaryKey: true
//     },
//     dept_name: Sequelize.STRING
// });

const Employees = require("./models/employees")(conn, Sequelize);
const Departments = require("./models/departments")(conn, Sequelize);

// conn.sync().then(function (err) {
//     console.log('Item table created successfully');
// });


const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));


app.get("/departments", function (req, res) {
    Departments.findAll().then(function (result) {
        res.status(200).json(result);
    })
});

app.get("/employees", function (req, res) {
    // Employees.findAll({limit:100}).then(function (result) {
    //     res.status(200).json(result);
    // })
    Employees.findAll({
        attributes: ["emp_no", "first_name"],
        where: { $and: [{ first_name: { $like: "William" } }, { last_name: { $like: "Ku" } }] },
        limit: 100
    }).then(function (result) {
        res.status(200).json(result);
    })
});


app.post("/employees", function (req, res) {
    const employee = req.body.employee;
    console.log(employee);
    Employees.create(employee).then(function (result) {
        res.status(201).send(result);
    }).catch(function (err) {
        console.log(err);
        res.status(422).send(err);
    });
});

app.put("/employees/:id", function (req, res) {
    const employee = req.body.employee;
    const id = req.params.id;
    Employees.update(employee, { where: { emp_no: id } }).then(function (result) {
        res.status(200).send(result);
    }).catch(function (err) {
        console.log(err);
        res.status(500).send(err);
    });
});

app.delete("/employees/:id", function (req, res) {
    // const employee = req.body.employee;
    const id = req.params.id;
    Employees.destroy({ where: { emp_no: id } }).then(function (result) {
        res.status(200).end();
    }).catch(function (err) {
        console.log(err);
        res.status(500).send(err);
    });
});



var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'iss-fsf'
});


// //using q
const mkQuery = function (sql, pool) {

    const sqlQuery = function () {
        const defer = q.defer();

        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, sqlParams, function (err, result) {
                if (err) {
                    defer.reject(err);
                    console.log(err);
                } else {
                    // console.log(result);
                    defer.resolve(result);
                }
                conn.release();
            });
        });

        return defer.promise;
    }

    return sqlQuery;
};

const SELECT_ALL_USERS = "select * from users";
const SELECT_USER_BY_ID = "select * from users where regid = ? limit 1";
const UPDATE_USER_BY_ID = "update users set name = ?, email = ?, phone = ?, dob = ? where regid = ?";


const getAllUsers = mkQuery(SELECT_ALL_USERS, pool);
const getUserById = mkQuery(SELECT_USER_BY_ID, pool);
const updateUserById = mkQuery(UPDATE_USER_BY_ID, pool);

function formatDate(date1) {
    return date1.getFullYear() + '-' +
        (date1.getMonth() < 9 ? '0' : "") + (date1.getMonth() + 1) + '-' +
        (date1.getDate() < 10 ? '0' : "") + date1.getDate();
}

const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});


app.post("/user/:id", function (req, res) {
    const id = req.params.id;
    const user = req.body.user;
    updateUserById(user.name, user.email, user.phone, formatDate(new Date(user.dob)), id)
        .then(function (result) {
            console.log(result);
            // res.status(404).type("text/plain").send("Customer not found");
            res.status(200).end();
        }).catch(function (err) {
            handleError(err, res);
        });
});



app.get("/users", function (req, res) {

    getAllUsers().then(function (result) {
        if (result.length > 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send("No users");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

app.get("/user/:id", function (req, res) {
    const id = req.params.id;
    getUserById(id).then(function (result) {
        if (result.length > 0) {
            var book = result[0];
            res.status(200).json(book);
        } else {
            res.status(404).send("User not found");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app