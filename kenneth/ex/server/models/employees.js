// Models employees table
module.exports = function (sequelize, Sequelize) {
    const Employees = sequelize.define('employees',
        {
            emp_no: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            birth_date: {
                type: Sequelize.DATE,
                allowNull: false
            },
            first_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            last_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            gender: {
                type: Sequelize.ENUM('M', 'F'),
                allowNull: false
            },
            hire_date: {
                type: Sequelize.DATE,
                allowNull: false
            }
        }, {
            timestamps: false,
            tableName: "employees"
        });

    return Employees;
};