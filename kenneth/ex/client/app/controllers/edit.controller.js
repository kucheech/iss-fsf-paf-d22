(function () {
    "use strict";
    angular.module("MyApp").controller("EditUserCtrl", EditUserCtrl);

    EditUserCtrl.$inject = ["MyAppService", "$state"];

    function EditUserCtrl(MyAppService, $state) {
        var vm = this; // vm
        vm.user = null;
        vm.id = 0;

        vm.init = function () {
            // console.log($state.params.id);
            vm.id = $state.params.id;
            // vm.showResults = false;

            MyAppService.getUserById(vm.id)
                .then(function (result) {
                    console.log(result);
                    vm.user = result;
                    vm.user.dob = new Date(vm.user.dob);
                }).catch(function (err) {
                    console.log(err);
                });

        }

        vm.init();

        vm.cancelEdit = function() {
            $state.go("users");
        }

        vm.saveEdit = function() {
            MyAppService.updateUser(vm.user)
            .then(function (result) {
                console.log(result);
                $state.go("users");
            }).catch(function (err) {
                console.log(err)
            });
        }

    }

})();