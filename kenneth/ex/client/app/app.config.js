(function () {
    "use strict";
    angular.module("MyApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function MyConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("users", {
                url: "/users",
                templateUrl: "/views/users.html"
            })
            .state("edit", {
                url: "/edit/:id",
                templateUrl: "/views/edit.html"
            })

        $urlRouterProvider.otherwise("users");
    }





})();